# =============================================================================
# @file    load-mock-data.py
# @brief   Load some sample data for DIBS
# @created 2021-01-07
# @license Please see the file named LICENSE in the project directory
# @website https://github.com/caltechlibrary/dibs
# =============================================================================

from datetime import datetime, timedelta
from decouple import config
from peewee import SqliteDatabase

from dibs.database import Item, Loan, History
from dibs.people import Person

db = SqliteDatabase(config('DATABASE_FILE', default='dibs.db'))

# Peewee autoconnects to the database if doing queries but not other ops.
db.connect()
db.create_tables([Item, Loan, History, Person])
