<!DOCTYPE html>
<html lang="en">
  %include('common/banner.html')
  <head>
    %include('common/standard-inclusions.tpl')
    <title>Error</title>
  </head>
  
  <body>
    <div class="page-content">
      %include('common/navbar.tpl')

      <div class="container main-container text-center">
        <h1 style="font-size: 5em;margin-top: 1em;">🤷🏻‍♂️</h1>
        <h4 class="my-4">Very sorry, but that seems to be missing &#8230;</h4>
        <p class="mx-auto" style="width: 85%">We hate when that happens! 
          Maybe it's been misplaced, or maybe it's really gone. Our
        staff will take note of the missing item. Our apologies for the
        inconvenience.</p>

        <p class="text-muted"><small>(Code {{code}}: {{message}})</small></p>
      </div>

      %include('common/footer.tpl')
    </div>
  </body>
</html>
