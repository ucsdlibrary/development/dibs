<!DOCTYPE html>
<html lang="en">
  %include('common/banner.html')
  <head>
    %include('common/standard-inclusions.tpl')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css">
    <title>About {{brand}}</title>
  </head>

  <body>
    <div class="page-content">
      %include('common/navbar.tpl')

      <div class="container main-container">
        <h1 class="mx-auto text-center pt-3 branding-color">
          About {{brand}}
        </h1>
        <h3 class="mx-auto text-center">
          <small class="text-muted font-italic font-weight-light">Version {{version}}</small>
        </h3>
        <p class="my-3"><strong>{{brand}}</strong>, a modified version of DIBS, allows members of UC San Diego to borrow materials that are not otherwise available in e-book or other electronic formats. Access to materials in {{brand_short}} is limited to current UC San Diego faculty, students and staff.</p>

        <p><em>DIBS is <a href="https://en.wikipedia.org/wiki/Open-source_software">open-source software</a>. The source code is freely available <a href="https://github.com/caltechlibrary/dibs">from GitHub</a> under the terms of a <a href="https://github.com/caltechlibrary/dibs/blob/main/LICENSE">BSD 3-clause license</a>.  It was designed and implemented by <a href="https://github.com/mhucka">Michael Hucka</a>, <a href="https://github.com/rsdoiel">Robert Doiel</a>, <a href="https://github.com/t4k">Tommy Keswick</a> and <a href="https://github.com/nosivads">Stephen Davison</a> of the Caltech Library's <a href="https://www.library.caltech.edu/staff?&field_directory_department%5B0%5D=754">Digital Library Development team</a>. DIBS is written primarily in Python and uses the <a href="http://universalviewer.io">Universal Viewer</a> and <a href="https://iiif.io">IIIF</a> for displaying scanned books and other items.  The icons used on DIBS web pages are from <a href="https://fontawesome.com">Font Awesome</a>, with additional special icons created by <a href="https://thenounproject.com/roywj/">Royyan Wijaya</a>, <a href="https://thenounproject.com/thezyna/">Scott Desmond</a> and <a href="https://thenounproject.com/slidegenius">SlideGenius</a> for the <a href="https://thenounproject.com">Noun Project</a>.</em></p>

      </div>

      %include('common/footer.tpl')
    </div>
  </body>
</html>
