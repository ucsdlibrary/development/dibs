% from datetime import datetime
% this_year = datetime.now().year

<!-- SERVICE BLURB -->
<section id="hc-service-blurb" class="container">
	<div class="row">
		<div class="col-12 alert alert-info" role="alert">

			<p>Welcome to the UC San Diego Library’s Controlled Digital Lending service! This is a new service for fall 2021, and makes available course materials that are otherwise not available electronically. This service is meant to increase the accessibility of course materials for students to access, no matter when or where they are.</p>
			<p>Course materials are limited to the number of print copies owned by the Library, in compliance with copyright best practices. We will also offer a single print copy for short-term use in the Reserves area of the Geisel Library, if you prefer to use those.</p>
			<p>Course materials in Controlled Digital Lending are not able to be copy/pasted, searched, or printed, in compliance with copyright laws. To continue being able to offer this service, we ask that all users abide by these policies, and avoid efforts to copy the materials or circumvent restrictions imposed by Digital Lending.</p>
			<p>You may only borrow a single item at a time, and must wait 30 minutes before you can check out that specific item again, to give other students a chance to use the material.</p>
			<p>Check-out times are generally three hours.</p>
			<p style="margin-bottom:0;">This is a new service, and we hope to grow and improve it! Please share your <a href="{{feedback_url}}">feedback</a> with us.</p>

		</div>
	</div>
</section>
<!-- /SERVICE BLURB -->

<!-- CAMPUS LIKE FOOTER -->
<footer id="hc-footer">
	<section class="container">
		<div class="row">
			<div class="col-sm-8">
				<p>
					<strong>{{brand}}</strong><br>
					<span>UC San Diego 9500 Gilman Dr. La Jolla, CA 92093 (858) 534-2230</span><br>
				<span>Copyright © <span class="footer-copyright-year">{{this_year}}</span> Regents of the University of California. All rights reserved.</span>
				</p>
				<ul class="footer-links">
					<li><a href="https://www.ucsd.edu/_about/legal/index.html">Terms & Conditions</a></li>
					<li><a href="{{feedback_url}}">Give Feedback</a></li>
					<li><a href="{{help_url}}">Help</a></li>
				</ul>
			</div>
			<div class="col-sm-4">
				<a href="https://www.ucsd.edu/" class="cd-footer-logo">
					<img alt="UC San Diego" class="img-responsive footer-logo" src="https://library.ucsd.edu/assets/libapps/shared/logo-ucsd-footer.png">
				</a>
			</div>
		</div>
	</section>
</footer>
<!-- /CAMPUS LIKE FOOTER -->