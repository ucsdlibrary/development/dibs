<!-- CAMPUS LIKE HEADER -->
<header id="hc-header" class="container">
	<section class="layout-title d-none d-sm-block">
		<div class="layout-container container">
			<div class="w-50">
				<a href="{{base_url}}" class="title-header title-header-large">{{brand_short}} <span style="font-size:50%;" class="badge badge-warning">BETA</span></a>
			</div>
			<div class="w-50">
				<a href="https://www.ucsd.edu/" class="title-logo">
					<img alt="UC San Diego" src="https://library.ucsd.edu/assets/libapps/shared/logo-ucsd-header.png">
				</a>
			</div>
		</div>
	</section>
</header>
<!-- /CAMPUS LIKE HEADER -->

<!-- CAMPUS LIKE-ISH NAVBAR -->
<nav class="navbar navbar-expand-lg navbar-dark UCSDify">
	<section class="container">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">

				%if staff_user:
					<li class="nav-item"><a class="nav-link" href="{{base_url}}/list">List Items</a></li>
					<li class="nav-item"><a class="nav-link" href="{{base_url}}/stats">Loan Statistics</a></li>
				%end
				<li class="nav-item"><a class="nav-link" href="{{base_url}}/about">About</a></li>

			</ul>

			%if logged_in:
				<form class="form-inline my-2 my-lg-0" action="{{base_url}}/logout" method="POST">
					<input class="btn btn-outline-light my-2 my-sm-0" type="submit" name="edit" value="Logout"/>
				</form>
			%end

		</div>
	</section>
</nav>
<!-- /CAMPUS LIKE-ISH NAVBAR -->
